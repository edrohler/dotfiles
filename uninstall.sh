#!/usr/bin/bash

rm  "$HOME/.bashrc"
rm  "$HOME/.bash_profile"
rm  "$HOME/.tmux.conf"
rm  "$HOME/.vimrc"
rm  "$HOME/.config/nvim/init.vim"
