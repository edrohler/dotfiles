# dotfiles

My dotfiles. Right now, I am only managing a few dotfiles. So, I just sumlink the 5 files I need and that's it. I don't have to worry about much else. If I do end up managing dozens of dotfiles I may move to [gnu stow](https://www.gnu.org/software/stow/).


## Symlinks
1. bashrc -> $HOME/.bashrc -> $HOME/.bash_profile
2. vimrc -> $HOME/.vimrc -> $HOME/.config/nvim/init.vim
3. tmux.conf -> $HOME/.tmux.conf
4. gtk.css -> $HOME/.config/gtk-3.0/gtk.css
5. config.conf -> $HOME/.config/neofetch/config.conf
