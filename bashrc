# .bashrc
~/src/repos/edr/pfetch/pfetch

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

PATH=$PATH:$HOME/.local/bin:$HOME/bin
export PATH

if [ -f `which powerline-daemon` ]; then
  powerline-daemon -q
  POWERLINE_BASH_CONTINUATION=1
  POWERLINE_BASH_SELECT=1
  . /usr/share/powerline/bash/powerline.sh
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=
# User specific aliases and functions

# pyenv setup
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
eval "$(pyenv virtualenv-init -)"

# rbenv setup
export PATH="$HOME/.rbenv/bin:$PATH"
export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
eval "$(rbenv init -)"

# nvm setup
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# go setup
export GOPATH=$HOME/go

# msssql server setup
export PATH="$PATH:/opt/mssql-tools/bin"

# Deno
export PATH="$PATH:$HOME/.deno/bin/"

export VISUAL=nvim
export VIMCONFIG=~/.vim
export VIMDATA=~/.vim
export FZF_DEFAULT_COMMAND="rg --files"

alias dnfupdate='sudo dnf update -y'
alias src="cd /home/$USER/src/"
alias blogsrc="cd /home/$USER/src/repos/edr/ericrohler_static/"
alias dotfilesrc="cd /home/$USER/src/repos/edr/dotfiles/"

###-tns-completion-start-###
if [ -f /home/eric/.tnsrc ]; then 
    source /home/eric/.tnsrc 
fi
###-tns-completion-end-###

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

