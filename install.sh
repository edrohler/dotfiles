#!/usr/bin/bash

# for f in $(ls --ignore="*.sh" --ignore="*.md" --ignore="*.css") 
# do
#     # echo $f
#     ln -s "$(pwd)/$f" "$HOME/.${f##*/}"
# done


ln -s "$(pwd)/bashrc"       "$HOME/.bashrc"
ln -s "$HOME/.bashrc"       "$HOME/.bash_profile"
ln -s "$(pwd)/tmux.conf"    "$HOME/.tmux.conf"
ln -s "$(pwd)/vimrc"        "$HOME/.vimrc"
ln -s "$HOME/.vimrc"        "$HOME/.config/nvim/init.vim"
